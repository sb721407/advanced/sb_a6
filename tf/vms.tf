##### VMs Part #####

resource "yandex_compute_instance" "server" {
  name        = "server"
  platform_id = "standard-v2"
  resources {
    core_fraction = 50
    cores         = "4"
    memory        = "4"
  }
  boot_disk {
    initialize_params {
      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "client" {
  name        = "client"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      #image_id = "fd87697emk1o3pa6iuva" # lemp
      size     = 5
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

##### Create file inventory #####

resource "local_file" "inventory" {
  content  = <<EOF

[server]
${yandex_compute_instance.server.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[client]
${yandex_compute_instance.client.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[consul_instances]
${yandex_compute_instance.server.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.server.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.server.network_interface.0.ip_address} 127.0.0.1" consul_node_role=server consul_bootstrap_expect=true
${yandex_compute_instance.client.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.client.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.client.network_interface.0.ip_address} 127.0.0.1" consul_node_role=client consul_enable_local_script_checks=true

EOF
  filename = "${path.module}/inventory"
}

##### Create file inventory 2 #####
resource "local_file" "consul_inv" {
  content  = <<-DOC

[consul_instances]
${yandex_compute_instance.server.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.server.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.server.network_interface.0.ip_address} 127.0.0.1" consul_node_role=server consul_bootstrap_expect=true
${yandex_compute_instance.client.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa consul_bind_address=${yandex_compute_instance.client.network_interface.0.ip_address} consul_client_address="${yandex_compute_instance.client.network_interface.0.ip_address} 127.0.0.1" consul_node_role=client consul_enable_local_script_checks=true

[client]
${yandex_compute_instance.client.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[server]
${yandex_compute_instance.server.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

    DOC
  filename = "../${path.module}/consul.inv"
}

resource "null_resource" "client" {
  depends_on = [yandex_compute_instance.client, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.client.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --private-key ~/.ssh/id_rsa ../ansible/pb-client.yml"
  }
}

resource "null_resource" "server" {
  depends_on = [yandex_compute_instance.server, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.server.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --private-key ~/.ssh/id_rsa ../ansible/pb-server.yml"
  }
}

resource "null_resource" "consul" {
  depends_on = [null_resource.client, null_resource.server, local_file.inventory]
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --private-key ~/.ssh/id_rsa ../ansible/site.yml"
  }
}

##### Add A-record to DNS zone #####

resource "yandex_dns_recordset" "app_dns_name" {
  depends_on = [yandex_compute_instance.server, local_file.inventory]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "s"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.server.network_interface.0.nat_ip_address]
}
